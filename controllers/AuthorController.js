const { Authors,Books } = require('../models')

// exports.landingPage = ((req,res,next)=>{
//     let findauthor;
//     Authors.findAll()
//     .then(author => {
//           findauthor = author;
            
//       })
//     // res.status(200).json(findauthor)
//     .catch(err =>{
//       console.log(err)
//     })
//     res.render('index', { title: 'Landing Page', display:'Home Page of', value: findauthor});
//     // res.status(202).json({findauthor})
//   })

exports.landingAuthor =  (async(req,res,next)=>{
  const foundAuthor = await Authors.findAll()
    res.render('index', { title: 'Landing Page', display:'List of authors', values: foundAuthor});
    // res.status(202).json({foundAuthor})
})
    
exports.createAuthor = (async(req,res,next)=>{
  const createdAuthor = await Authors.create({
    name: req.body.name,
    email: req.body.email,
  })
  res.redirect('/authors')
})

exports.updateAuthor = (async(req,res,next)=>{
    // try {
      const foundAuthor = await Authors.findByPk(req.params.id);
      const updatedAuthor = await foundAuthor.update({
        name : req.body.name,
        email: req.body.email,
      }, {
        where: {
          id: req.params.id
        }
      });
      res.redirect('/authors')
    // } catch (error) {
      // res.render('error', {message:'ERROR!!!', error:error})
    // };
});

exports.deleteAuthor =(async (req, res) => {
  await Authors.destroy({
      where: {
          id: req.params.id
      }
  })
  res.redirect('/authors')
})


exports.landingBook =  (async(req,res,next)=>{
  const foundBook = await Books.findAll()
    res.render('books', { title: 'Landing Page for Books', display:'List of Books', values: foundBook});
    // res.status(202).json({foundAuthor})
})

exports.createBook = (async(req,res,next)=>{
  try {
    const createdBook = await Books.create({
      title: req.body.title,
      page: req.body.page,
      price: req.body.price,
      author_id: req.body.author_id
    })
    res.redirect('/books')
  } catch (err){
    res.json(err)
  }
  })
  

exports.updateBook = (async(req,res,next)=>{
  // try {
    const foundBook = await Books.findByPk(req.params.id);
    const updatedBook = await foundBook.update({
      title: req.body.title,
      page: req.body.page,
      price: req.body.price,
      author_id: req.body.author_id
    }, {
      where: {
        id: req.params.id
      }
    });
    res.redirect('/books')
  // } catch (error) {
    // res.render('error', {message:'ERROR!!!', error:error})
  // };
});
    
exports.deleteBook =(async (req, res) => {
  await Books.destroy({
      where: {
          id: req.params.id
      }
  })
  res.redirect('/books')
})
