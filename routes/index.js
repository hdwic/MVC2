var express = require('express');
var router = express.Router();


//using controller for index
const indexController = require('../controllers/AuthorController.js')


router.get('/authors', indexController.landingAuthor);
router.post('/authors',indexController.createAuthor);
router.put('/authors/:id', indexController.updateAuthor);
router.delete('/authors/:id', indexController.deleteAuthor);

router.get('/books', indexController.landingBook);
router.post('/books',indexController.createBook);
router.put('/books/:id', indexController.updateBook);
router.delete('/books/:id', indexController.deleteBook);

module.exports = router;
